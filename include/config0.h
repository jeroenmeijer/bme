#include <ESP8266WiFi.h>
// Update these with values suitable for your MQTT server and account

// Station
#define homeSsid "SSID"
#define homePassword "PASSWORD"
#define DHCP
//IPAddress ip(172, 19, 3, 212);
//IPAddress gateway(172, 19, 3, 254);
//IPAddress subnet(255, 255, 255, 0);
//IPAddress dns(172, 19, 3, 251);

// MQTT broker
// #define mqtt_ssl
#define mqtt_server "mqttserver"
#define mqtt_port 1883
#define mqtt_username "USERNAME"
#define mqtt_password "PASSWORD"
#define mqtt_clientId "bme280-1"
#define mqtt_topic "stat/scotty/data"

// OTA update
#define HTTP_OTA_ADDRESS F("otaserver")      // Address of OTA update server
#define HTTP_OTA_PATH F("/esp8266-ota/update/update.php") // Path to update firmware
#define HTTP_OTA_PORT 8123                                // Port of update server
#define HTTP_OTA_NAME F("scotty")

// Vcc factor
#define vcc_fac 1101

// BME pins
#define I2C_SDA 2
#define I2C_SCL 0
