#define DEBUG 1
#define SERIAL_BPS 115200
#define VERSION "014"

/* settings:
 *  Generic ESP8266
 *  Flash mode QIO
 *  Flash size 4MB (3MB SPIFFS)
 *  */

#ifdef CONFIG0
#include "config0.h"
#endif
#ifdef CONFIG1
#include "config1.h"
#endif
#ifdef CONFIG2
#include "config2.h"
#endif
#ifdef CONFIG3
#include "config3.h"
#endif
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <ESP8266WiFi.h>
#include <ESP8266httpUpdate.h>
#include <PubSubClient.h>

#define RTC_OK 0
#define RTC_NOWIFI 1
#define RTC_NOMQTT 2
#define RTC_UPDATED 3
#define RTC_NOPUBLISH 4
#define RTC_OTAFAIL 5

char msgBuf[128];
uint32_t rtc;

Adafruit_BME280 bme; // Sensor
TwoWire w2;          // I2C

ADC_MODE(ADC_VCC); // ADC coupled to battery voltage

#ifdef mqtt_ssl
WiFiClientSecure wiFiClient;
#endif

#ifndef mqtt_ssl
WiFiClient wiFiClient;
#endif

PubSubClient pubSubClient(wiFiClient);

template <typename Generic>
void debugMsgLn(Generic g)
{
#ifdef DEBUG
  Serial.println(g);
#endif
}

template <typename Generic>
void debugMsg(Generic g)
{
#ifdef DEBUG
  Serial.print(g);
#endif
}

int WifiGetRssiAsQuality(int rssi)
{
  int quality = 0;

  if (rssi <= -100)
  {
    quality = 0;
  }
  else if (rssi >= -50)
  {
    quality = 100;
  }
  else
  {
    quality = 2 * (rssi + 100);
  }
  return quality;
}

void setup()
{

  bool status;
  char miniBuf[10];

#ifdef DEBUG
  Serial.begin(SERIAL_BPS);
#endif
  debugMsgLn(F(""));
  debugMsg(F("Weather sensor "));
  debugMsgLn(VERSION);

  // Start I2C wire core on ESP-01 compatible pins. 0:SDA, 1:SCL.
  w2.begin(I2C_SDA, I2C_SCL);

  // Start the BMP sensor.
  status = bme.begin(0x76, &w2);
  if (!status)
  {
    debugMsgLn(F("Could not find a valid BME280 sensor, check wiring!"));
    delay(100);
    return;
  }
  else
  {
    // Put the BME in "weather station" mode (sampling stops,
    // no oversampling, no filters).
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X1, // temperature
                    Adafruit_BME280::SAMPLING_X1, // pressure
                    Adafruit_BME280::SAMPLING_X1, // humidity
                    Adafruit_BME280::FILTER_OFF);

    // Make one measurement and wait for completion.
    // The BME will go into low power mode when complete
    // but the values will be available through the
    // read.... methods.
    bme.takeForcedMeasurement();
  }

  // Now start the WiFi https://www.bakke.online/index.php/2017/05/22/reducing-wifi-power-consumption-on-esp8266-part-3/
  debugMsgLn(F("Initialising Wifi..."));
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepWake();
  delay(1);
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
#ifndef DHCP
  WiFi.config(ip, gateway, subnet, dns);
#endif
  // Connect to the WiFi.
  if (WiFi.status() != WL_CONNECTED)
  { // should always be true
    debugMsgLn(F("WiFi.begin"));
    WiFi.begin(homeSsid, homePassword);
  }
  int c = 100; // give it 10 seconds.
  while (c-- && WiFi.status() != WL_CONNECTED)
    delay(100);

  // Gracefully handle failure, record, and fall asleep.
  if (WiFi.status() != WL_CONNECTED)
  {
    rtc = RTC_NOWIFI; // No wifi connection
    debugMsgLn(F("WiFi connection Failed!"));
    delay(100);
    return; // quit setup, so it will enter loop and promptly fall asleep
  }

  // Report connected (debug). Data only relevant when using DHCP.
  debugMsg(F("Connected to: "));
  debugMsg(WiFi.SSID());
  debugMsg(F(", IP address: "));
  debugMsgLn(WiFi.localIP());

  // Connect to the MQTT broker.
  pubSubClient.setServer(mqtt_server, mqtt_port);
  //pubSubClient.setCallback(callback);

  // Gracefully handle failure, record, and fall asleep.
  if (!pubSubClient.connect(mqtt_clientId, mqtt_username, mqtt_password))
  {
    rtc = RTC_NOMQTT; // Connect to WiFi, but not to broker
    debugMsgLn(F("MQTT connection Failed!"));
    delay(100);
    return; // quit setup, so it will enter loop and promptly fall asleep
  }

  // Report MQTT connected (debug).
  debugMsgLn(F("MQTT connected"));

  // Report Vcc to MQTT and open JSON.
  strcpy(msgBuf, "{\"vcc\":");
  itoa((int)((long)ESP.getVcc() * vcc_fac / 1000), miniBuf, 10);
  strcat(msgBuf, miniBuf);

  // Report last status to MQTT in case one or more were missed.
  strcat(msgBuf, ",\"prev\":");
  ESP.rtcUserMemoryRead(0, &rtc, sizeof(rtc));
  itoa((int)rtc, miniBuf, 10);
  strcat(msgBuf, miniBuf);

  // Report temperature to MQTT.
  strcat(msgBuf, ",\"temp\":");
  snprintf(miniBuf, 64, "%0.2f", status ? bme.readTemperature() : -999.0F);
  strcat(msgBuf, miniBuf);

  // Report pressure to MQTT.
  strcat(msgBuf, ",\"baro\":");
  snprintf(miniBuf, 64, "%0.2f", status ? bme.readPressure() / 100.0F : -999.0F);
  strcat(msgBuf, miniBuf);

  // Report humidity to MQTT.
  strcat(msgBuf, ",\"humi\":");
  snprintf(miniBuf, 64, "%0.2f", status ? bme.readHumidity() : -999.0F);
  strcat(msgBuf, miniBuf);

  // Report RSSI to MQTT.
  strcat(msgBuf, ",\"RSSI\":\"");
  itoa((int)WifiGetRssiAsQuality(WiFi.RSSI()), miniBuf, 10);
  strcat(msgBuf, miniBuf);
  strcat(msgBuf, "\"");

  // Report version to MQTT.
  strcat(msgBuf, ",\"version\":\"");
  strcat(msgBuf, VERSION);
  strcat(msgBuf, "\"");

  // End JSON string.
  strcat(msgBuf, "}");

  // Publish the JSON string. Retain the message
  rtc = RTC_OK; // assume all OK
  if (!pubSubClient.publish(mqtt_topic, msgBuf, true))
  {                      // do not retain
    rtc = RTC_NOPUBLISH; // missed a publish
  }

  // Disconnect from MQTT
  delay(10);
  pubSubClient.disconnect();
}

void loop()
{

  // try http ota
#ifdef HTTP_OTA_ADDRESS
  if (WiFi.status() == WL_CONNECTED)
  {
    debugMsg(F("Check OTA..."));
	debugMsgLn (String(HTTP_OTA_NAME) + String(".") + String(VERSION));
    // now try OTA
    switch (ESPhttpUpdate.update(wiFiClient, HTTP_OTA_ADDRESS, HTTP_OTA_PORT, HTTP_OTA_PATH, String(HTTP_OTA_NAME) + String(".") + String(VERSION)))
    {
    case HTTP_UPDATE_FAILED:
      debugMsgLn("HTTP update failed: Error " + ESPhttpUpdate.getLastErrorString() + "\n");
      rtc = RTC_OTAFAIL;
      break;
    case HTTP_UPDATE_NO_UPDATES:
      debugMsgLn(F("No updates"));
      break;
    case HTTP_UPDATE_OK:
      debugMsgLn(F("Update OK"));
      rtc = RTC_UPDATED;
      break;
    }
  }
#endif

  // save last status
  ESP.rtcUserMemoryWrite(0, &rtc, sizeof(rtc));

  delay(10);
  WiFi.mode(WIFI_OFF);
  delay(10);
  debugMsgLn(F("Zzz..."));
#ifdef DEBUG
  delay(100);
  Serial.end();
#endif
  ESP.deepSleep(300 * 1e6, WAKE_RF_DEFAULT); // 5 minutes
  delay(10000);                              // give it time to fall asleep
}
